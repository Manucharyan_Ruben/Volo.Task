﻿

function MyConfirm(message, actionLink) {
    swal({
        title: "Are you sure?",
        text: message,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    },
function () {
    location.href = actionLink;
    swal("Deleted!", "Your imaginary file has been deleted.", "success");
})
}

function MySuccessMessage(message) {
    swal(message,"", "success")
}