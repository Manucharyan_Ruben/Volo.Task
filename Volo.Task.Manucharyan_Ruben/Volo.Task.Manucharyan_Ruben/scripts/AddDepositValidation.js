﻿

function IsValid(depositType) {

    var realSpan = GetTimeSpan();

    if (document.getElementById("DepositType").value == "Select deposit type") {
        document.getElementById("DepositType").className += " input-validation-error";
        document.getElementById("validationMessageForDepositType").hidden = false;
    }
    else if ((realSpan < depositType.MinTerm || realSpan > depositType.MaxTerm) || isNaN(realSpan)) {
        document.getElementById("Deadline").classList.add("input-validation-error");
        document.getElementById("validationMessageForDeadline").innerText = "";
        document.getElementById("validationMessageForDeadline").innerText += "Deadline term must be between " + depositType.MinTerm + " days and " + depositType.MaxTerm + " days.";
        document.getElementById("validationMessageForDeadline").hidden = false;

        document.getElementById("validationMessageForDepositType").hidden = true;
    }
    else if (document.getElementById("Amount").value < depositType.MinAmount || document.getElementById("Amount").value > depositType.MaxAmount) {
        document.getElementById("Amount").className += " input-validation-error";
        document.getElementById("validationMessageForAmount").hidden = false;
        document.getElementById("validationMessageForAmount").innerText = "";
        document.getElementById("validationMessageForAmount").innerText += "Amount must be between " + depositType.MinAmount + " and " + depositType.MaxAmount;

        document.getElementById("addDeposit").disabled = false;
        document.getElementById("validationMessageForDepositType").hidden = true;
        document.getElementById("validationMessageForDeadline").hidden = true;
    }
    else {
        document.getElementById("addDeposit").disabled = false;
        document.getElementById("validationMessageForDepositType").hidden = true;
        document.getElementById("validationMessageForDeadline").hidden = true;
        document.getElementById("validationMessageForAmount").hidden = true;
        return true;
    }
    document.getElementById("addDeposit").disabled = true;
     
    return false;
}

function ReckonProfit() {

    if (IsValid(depositTypes[0])) {



        var realSpan = GetTimeSpan();

        var percentPerDay = depositTypes[0].InterestRate / 365;
        var amount = document.getElementById("Amount").value;
        var result = amount * (realSpan * (percentPerDay / 100));


        document.getElementById("Profit").innerHTML = result.toFixed(2);
    }
}

function GetTimeSpan()
{
    var startdate = new Date();
    var deadline = new Date(document.getElementById("Deadline").value)
    var span = deadline - startdate;
    var oneDay = 1000 * 60 * 60 * 24
    return realSpan = parseInt((span / oneDay) +1);
}
