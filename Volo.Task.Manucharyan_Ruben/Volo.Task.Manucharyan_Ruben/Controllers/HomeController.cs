﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Volo.Task.Database.Entities;
using Volo.Task.Database.Services;

namespace Volo.Task.Manucharyan_Ruben.Controllers
{
    public class HomeController : Controller
    {               
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}