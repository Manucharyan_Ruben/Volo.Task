﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
namespace Volo.Task.Manucharyan_Ruben.App_Start
{
    public class BundleConfig
    {
        
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                          "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*")
                        .Include("~/scripts/IconChangeScript.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*")
                        .Include("~/Scripts/sweetalert.min.js")
                        .Include("~/Scripts/Script.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css").Include("~/Content/sweetalert.css"));

            bundles.Add(new ScriptBundle("~/AddDepositVal").Include(
                 "~/Scripts/AddDepositValidation.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajax")
                .Include("~/Content/jquery.unobtrusive-ajax.min.js"));
        }
    }
    
}