﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Volo.Task.Manucharyan_Ruben.HtmlHelpers
{
    public static class PaginationHelper
    {
        public static MvcHtmlString AddPegination(this HtmlHelper html, int pageCount, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i < pageCount+1; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
             
                tag.AddCssClass("btn btn - default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}