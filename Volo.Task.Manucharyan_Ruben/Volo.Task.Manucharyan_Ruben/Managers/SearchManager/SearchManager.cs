﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Task.Database.DatabaseManager;
using Volo.Task.Database.DepositTypes;
using Volo.Task.Database.Entities;

namespace Volo.Task.Manucharyan_Ruben.Managers.SearchManager
{
    public class SearchManager : ISearchManager
    {
        /// <summary>
        /// Makes search
        /// </summary>
        /// <param name="clientsFirstName">can be null</param>
        /// <param name="clientsLastName">can be null</param>
        /// <param name="depositType">To see all the deposit types, it must be "All"</param>
        /// <param name="banksName">can be null</param>
        /// <param name="minAmount">can be null</param>
        /// <param name="maxAmount">can be null</param>
        /// <param name="minStartDate">can be null</param>
        /// <param name="maxStartDate">can be null</param>
        /// <returns>Returns filtered results. If all values are null and deposit type is "All", then returns  all values</returns>
        public async Task<dynamic> Search(string clientsFirstName, string clientsLastName,string depositType, string banksName,
              decimal? minAmount, decimal? maxAmount, DateTime? minStartDate, DateTime? maxStartDate)
        {
            IDatabaseManager dbManager = new DatabaseManager();
            List<Deposit> deposits = await dbManager.GetDeposits();

            List<Bank> banks = await dbManager.GetBanks();
            List<Client> clients = await dbManager.GetClients();

            var result = deposits.Join(banks, d => d.BankId, b => b.Id, (d, b) => new { Deposit = d, Bank = b })
                .Join(clients, r => r.Deposit.ClientId, c => c.Id, (r, c) => new { Deposit = r.Deposit, Bank = r.Bank, Client = c })
                .Where(r => (string.IsNullOrWhiteSpace(banksName) || r.Bank.Name.ToLower() == banksName.Trim(' ').ToLower())
                && (string.IsNullOrWhiteSpace(clientsFirstName) || r.Client.FirstName.ToLower() == clientsFirstName.ToLower()) && (string.IsNullOrWhiteSpace(clientsLastName)
                || r.Client.LastName.ToLower() == clientsLastName.Trim(' ').ToLower()) && (r.Deposit.Amount >= minAmount || minAmount == null) && (r.Deposit.Amount <= maxAmount
                || maxAmount == null) && (r.Deposit.StartDate >= minStartDate || minStartDate == null) && (r.Deposit.StartDate <= maxStartDate
                || maxStartDate == null) && (AllDeposits.GetAll.Where(dt => dt.Id == r.Deposit.DepositId).First().Name == depositType
                || depositType=="All")).ToList();

            return result;

        }

    }
}