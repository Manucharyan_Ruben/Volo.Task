﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Volo.Task.Manucharyan_Ruben.Managers.SearchManager
{
    public interface ISearchManager
    {
        Task<dynamic> Search(string clientsFirstName, string clientsLastName, string depositType, string banksName,
           decimal? minAmount, decimal? maxAmount, DateTime? minStartDate, DateTime? maxStartDate);
    }
}