﻿using System;
using Volo.Task.Database.DepositTypes;

namespace Volo.Task.Manucharyan_Ruben.Managers.DepositManager
{
    public interface IDepositManager
    {
        System.Threading.Tasks.Task AddDeposit(int clientId, int bankId, int depositId, decimal amount, DateTime deadline, decimal profit);
        bool CanAddDeposit(DateTime startDate, DateTime deadline, DepositType depositType, decimal amount);
        decimal Profit(DateTime startDate, DateTime deadline, DepositType depositType, decimal amount);
    }
}