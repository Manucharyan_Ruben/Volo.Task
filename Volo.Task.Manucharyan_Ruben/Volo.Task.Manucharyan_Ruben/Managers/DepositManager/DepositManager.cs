﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Volo.Task.Database.DatabaseManager;
using Volo.Task.Database.DepositTypes;
using Volo.Task.Database.Entities;

namespace Volo.Task.Manucharyan_Ruben.Managers.DepositManager
{
    public class DepositManager : IDepositManager
    {
        /// <summary>
        /// Adds deposit for the client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="bankId"></param>
        /// <param name="depositId"></param>
        /// <param name="amount"></param>
        /// <param name="deadline"></param>
        /// <param name="profit"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task AddDeposit(int clientId, int bankId, int depositId, decimal amount, DateTime deadline, decimal profit)
        {
            IDatabaseManager dbManager = new DatabaseManager();
            Deposit deposit = new Deposit
            {
                BankId = bankId,
                ClientId = clientId,
                DepositId = depositId,
                Amount = amount,
                StartDate = DateTime.Now,
                Deadline = deadline,
                Profit = profit
            };
            await dbManager.AddDeposit(deposit);

        }

        /// <summary>
        /// Calculates the profit
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="deadline"></param>
        /// <param name="depositType"></param>
        /// <param name="amount"></param>
        /// <returns>Returns the profit</returns>
        public decimal Profit(DateTime startDate, DateTime deadline, DepositType depositType, decimal amount)
        {
            TimeSpan timeSpan = deadline - startDate;
            int days = timeSpan.Days;
            decimal percentPerDay = (decimal)depositType.InterestRate / 365;
            return amount * (days * (percentPerDay / 100));
        }

        /// <summary>
        /// Checks whether the deposit data corresponds to the given deposit type, or not
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="deadline"></param>
        /// <param name="depositType"></param>
        /// <param name="amount"></param>
        /// <returns>true if corresponds, else false</returns>
        public bool CanAddDeposit(DateTime startDate, DateTime deadline, DepositType depositType, decimal amount)
        {

            TimeSpan timeSpan = deadline - startDate;
            int days = timeSpan.Days;
            if ((days >= depositType.MinTerm && days <= depositType.MaxTerm) && (amount >= depositType.MinAmount && amount <= depositType.MaxAmount))
            {
                return true;
            }

            return false;
        }
    }
}