﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Task.Manucharyan_Ruben.Managers.MessageManager
{
    /// <summary>
    /// Messages exposed to the user
    /// </summary>
    public static class Messages
    {
        public const string ClientAddedSuccessfully = "Client added successfully";
        public const string BankAddedSuccessfully = "Bank added successfully";
        public const string DepositAddedSuccessfully = "Deposit added successfully";
        public const string BankSuccessfullyEdited = "Bank successfully edited";
        public const string ClientSuccessfullyEdited = "Client successfully edited";
        public const string BankIsAlreadyExist = "Bank is already exist";
        public const string ClientIsAlreadyExist = "Client is already exist";
        public const string ClientAlreadyHaveADepositInThisBank = "Client already have a deposit in this bank";
        public const string BankDeleted = "Bank deleted";
    }
}