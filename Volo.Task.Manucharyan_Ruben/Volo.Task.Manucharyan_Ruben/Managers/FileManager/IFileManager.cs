﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Task.Manucharyan_Ruben.Managers.FileManager
{
    public interface IFileManager
    {
        string SaveFile(HttpPostedFileBase file, string mapPath);
        void RenameFile(string oldName, string newName, string mapPath);
    }
}