﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Volo.Task.Manucharyan_Ruben.Managers.FileManager
{
    public class FileManager : IFileManager
    {
        private List<string> imageExtensions;

        public FileManager()
        {
            imageExtensions = new List<string>
            {
                ".jpeg",
                ".jpg",
                ".ico",
                ".gif",
                ".png",
                ".tif"
            };
        }

        /// <summary>
        /// Checks whether the file that has come is an image or not
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public  bool IsPicture(string extension)
        {
            if (imageExtensions.Contains(extension))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Saves the file
        /// </summary>
        /// <param name="file"></param>
        /// <param name="mapPath"></param>
        /// <returns>If the file has been successfully saved, then it returns the file extension, else - null</returns>
        public string SaveFile(HttpPostedFileBase file, string mapPath)
        {
            string extension = Path.GetExtension(file.FileName);

            if (IsPicture(extension))
            {
                foreach (var item in imageExtensions)
                {

                    if (File.Exists(mapPath + item))
                    {
                         File.Delete(mapPath + item);
                        break;
                    }
                }
                
                file.SaveAs(mapPath + extension);
                return extension;
            }
            return null;
        }

        /// <summary>
        /// Changes the file name 
        /// </summary>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>
        /// <param name="mapPath"></param>
        public void RenameFile(string oldName, string newName, string mapPath)
        {
            foreach (var item in imageExtensions)
            {
                if (File.Exists(mapPath+oldName+item))
                {
                    File.Move(mapPath + oldName + item, mapPath + newName + item);
                    break;
                }
            }
        }

    }
}