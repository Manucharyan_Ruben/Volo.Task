﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Volo.Task.Database.DepositTypes;
using Volo.Task.Database.Entities;
using Volo.Task.Manucharyan_Ruben.Managers.DepositManager;
using Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel;
using Volo.Task.Manucharyan_Ruben.Managers.MessageManager;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.Controllers
{
    public class DepositController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> AddDeposit(int id)
        {
            return View(await CreateDepositModelForView(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddDeposit(DepositModel depositModel)
        {
            ModelState["DepositType"].Errors.Clear();
            if (ModelState.IsValid)
            {
                depositModel.BankId = (await _databaseManager.GetBanks()).Where(b => b.Name == Request.Form["BankName"]).Last().Id;

                var deposits = (await _databaseManager.GetDeposits()).Where(d => d.ClientId == depositModel.ClientId && d.BankId == depositModel.BankId);
                if (deposits.Count() != 0)
                {
                    ModelState.AddModelError("", Messages.ClientAlreadyHaveADepositInThisBank);
                    return View(await CreateDepositModelForView(depositModel.ClientId));
                }

                depositModel.DepositType = AllDeposits.GetAll.Where(d => d.Name == Request.Form["DepositType"]).Last();

                depositModel.Deadline = depositModel.Deadline.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second + 1);

                IDepositManager depositManager = new DepositManager();

                if (depositManager.CanAddDeposit(DateTime.Now, depositModel.Deadline, depositModel.DepositType, depositModel.Amount))
                {
                    decimal profit = depositManager.Profit(DateTime.Now, depositModel.Deadline, depositModel.DepositType, depositModel.Amount);

                    await depositManager.AddDeposit(depositModel.ClientId, depositModel.BankId, depositModel.DepositType.Id, depositModel.Amount, depositModel.Deadline, profit);

                    TempData["useAsPartialWithMessage"] = true;
                    return RedirectToAction("All");
                }
                else
                {
                    ViewBag.SomethingWrong = true;

                    ViewBag.depositType = depositModel.DepositType;

                    return View(await CreateDepositModelForView(depositModel.ClientId));
                }
            }
            else
            {
                return View(await CreateDepositModelForView(depositModel.ClientId));
            }
        }

        [HttpGet]
        public ActionResult DepositInfo(string depositName)
        {
            return Json(AllDeposits.GetAll.Where(d => d.Name == depositName || depositName == "Select deposit type"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> All(int? id, int page = 1)
        {
            IEnumerable<Deposit> deposits =( await _databaseManager.GetDeposits()).Skip((page - 1) * 10).Take(10); 
            IEnumerable<Bank> banks = await _databaseManager.GetBanks();
            IEnumerable<Client> clients = await _databaseManager.GetClients();

            IEnumerable<DepositModel> depositsModel = DepositModel.GetAllDeposits(deposits.Where(d => id == null || d.BankId == id), clients, banks);
                                                                                                 

            ViewBag.PageCount = (deposits.Count() / 10) + 1;

            bool useAsPartialWithMessage = (bool?)TempData["useAsPartialWithMessage"] ?? false;

            if (useAsPartialWithMessage)
            {
                ViewBag.Message = Messages.DepositAddedSuccessfully;
            }


            return View("AllDeposits", depositsModel);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int depositId)
        {
            Deposit deposit = await _databaseManager.GetDeposit(depositId);
            deposit.Status = DepositStatus.Deleted.ToString();
            await _databaseManager.DeleteDeposit(depositId);

            return RedirectToAction("All");
        }

        [HttpGet]
        private async Task<DepositModel> CreateDepositModelForView(int clientId)
        {
            Client client = await _databaseManager.GetClient(clientId);

            List<Bank> banks = await _databaseManager.GetBanks();

            return new DepositModel
            {
                Banks = banks,
                Client = client
            };
        }

    }
}
