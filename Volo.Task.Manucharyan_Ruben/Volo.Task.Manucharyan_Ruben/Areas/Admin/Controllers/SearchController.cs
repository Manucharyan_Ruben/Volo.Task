﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Task.Database.Entities;
using Volo.Task.Manucharyan_Ruben.Managers.SearchManager;
using Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel;
using System.Threading.Tasks;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.Controllers
{
    public class SearchController : BaseController
    {
        [HttpGet]
        public ActionResult Search()
        {
            return View(new SearchModel().DepositTypes);
        }

        [HttpPost]
        public async Task<ActionResult> Search(SearchModel searchModel)
        {

            ISearchManager searchManager = new SearchManager();
            dynamic result = await searchManager.Search(searchModel.FirstName, searchModel.LastName, searchModel.DepositType,
                 searchModel.BankName, searchModel.MinAmount, searchModel.MaxAmount, searchModel.MinStartDate, searchModel.MaxStartDate);


            SearchModel searchViewModel = new SearchModel(result);

            return PartialView("_SearchResult", searchViewModel.SearchResult);
        }
    }
}