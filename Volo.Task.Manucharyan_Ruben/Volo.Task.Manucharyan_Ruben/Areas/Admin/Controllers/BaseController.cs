﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Task.Database.DatabaseManager;
using Volo.Task.Database.Entities;
using Volo.Task.Database.Services;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IDatabaseManager _databaseManager;

        public BaseController() : this(new DatabaseManager())
        {
        }

        public BaseController(IDatabaseManager databaseManager)
        {
            _databaseManager = databaseManager;
        }
    }
}