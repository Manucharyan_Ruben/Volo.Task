﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Volo.Task.Database.Entities;
using Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel;
using Volo.Task.Manucharyan_Ruben.Managers.MessageManager;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.Controllers
{
    public class ClientController : BaseController
    {
        [HttpGet]
        public ActionResult AddClient()
        {
            return View(new ClientModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddClient(ClientModel clientModel)
        {
            if (ModelState.IsValid)
            {
                IEnumerable<Client> client = (await _databaseManager.GetClients()).Where(c => c.Passport == clientModel.Passport
                                                                            || c.Email == clientModel.Email || c.Phone == clientModel.Phone);

                if (client.Count() != 0)
                {
                    ModelState.AddModelError("", $"{client.Last().FirstName} {client.Last().LastName} {Messages.ClientIsAlreadyExist}");
                    TempData["useAsPartial"] = true;
                    ViewBag.UseAsPartial = (bool?)TempData["useAsPartial"] ?? false;
                    return View(clientModel);
                }

                Client newClient = new Client
                {
                    FirstName = clientModel.FirstName,
                    LastName = clientModel.LastName,
                    Birthday = clientModel.Birthday,
                    Passport = clientModel.Passport,
                    Email = clientModel.Email,
                    Phone = clientModel.Phone
                };

                await _databaseManager.AddClient(newClient);
                TempData["useAsPartialWithMessage"] = true;
                return RedirectToAction("AllClients");
            }
            return View(clientModel);
        }

        [HttpGet]
        public async Task<ActionResult> AllClients(int page = 1)
        {
            IEnumerable<Client> clients = (await _databaseManager.GetClients()).Skip((page - 1) * 10).Take(10);
            IEnumerable<ClientModel> clientsModel = ClientModel.GetAllClients(clients);

            ViewBag.PageCount = (clients.Count() / 10) + 1;

            bool useAsPartialWithMessage = (bool?)TempData["useAsPartialWithMessage"] ?? false;

            if (useAsPartialWithMessage)
            {
                ViewBag.UseAsPartial = useAsPartialWithMessage;
                ViewBag.Message = Messages.ClientAddedSuccessfully;
            }
            return View(clientsModel);
        }

        [HttpGet]
        public async Task<ActionResult> EditClient(int id)
        {
            Client client = await _databaseManager.GetClient(id);
            ClientModel clientModel = ClientModel.GetClientModel(client);
            return View(clientModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditClient(ClientModel clientModel)
        {
            if (ModelState.IsValid)
            {
                Client client = new Client
                {
                    FirstName = clientModel.FirstName,
                    LastName = clientModel.LastName,
                    Birthday = clientModel.Birthday,
                    Passport = clientModel.Passport,
                    Email = clientModel.Email,
                    Phone = clientModel.Phone,
                    Id = clientModel.Id
                };

                await _databaseManager.ChangeClient(client);

                TempData["givAMessage"] = true;
                return RedirectToAction("ClientPage", new { id = client.Id });
            }
            return View(clientModel);
        }

        [HttpGet]
        public async Task<ActionResult> ClientPage(int id)
        {
            Client client = await _databaseManager.GetClient(id);
            ClientModel clientModel = ClientModel.GetClientModel(client);

            bool sentAMessage = (bool?)TempData["givAMessage"] ?? false;

            if (sentAMessage)
            {
                ViewBag.Message = Messages.ClientSuccessfullyEdited;
            }

            return View(clientModel);
        }

        [HttpGet]
        public async Task<ActionResult> DeleteClient(int id)
        {
            await _databaseManager.DeleteClient(id);
            return RedirectToAction("AllClients");
        }
    }
}