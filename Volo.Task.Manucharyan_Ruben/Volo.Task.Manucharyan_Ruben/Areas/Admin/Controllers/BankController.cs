﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Volo.Task.Database.DepositTypes;
using Volo.Task.Database.Entities;
using Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel;
using Volo.Task.Manucharyan_Ruben.Managers.FileManager;
using Volo.Task.Manucharyan_Ruben.Managers.MessageManager;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.Controllers
{

    public class BankController : BaseController
    {
        [HttpGet]
        public ActionResult AddBank()
        {
            return View(new BankModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddBank(BankModel bankModel, HttpPostedFileBase Ico)
        {
            if (ModelState.IsValid)

            {
                var banks = (await _databaseManager.GetBanks()).Where(b => b.Name == bankModel.Name);
                if (banks.Count() != 0)
                {
                    ModelState.AddModelError("", Messages.BankIsAlreadyExist);
                    return View(bankModel);
                }

                string icoExtension = string.Empty;
                if (Ico != null)
                {
                    IFileManager fileManager = new FileManager();
                    icoExtension = fileManager.SaveFile(Ico, Server.MapPath($"/Content/Images/Banks_Icons/{bankModel.Name.Trim(' ')}"));
                }

                Bank bank = new Bank()
                {
                    Name = bankModel.Name,
                    Description = bankModel.Description,
                    Ico = icoExtension != string.Empty ? $"/Content/Images/Banks_Icons/{bankModel.Name}{icoExtension}" : string.Empty
                };

                await _databaseManager.AddBank(bank);
                TempData["Message"] = Messages.BankAddedSuccessfully;
                return RedirectToAction("AllBanks");
            }

            else
            {
                return PartialView(bankModel);
            }
        }

        [HttpGet]
        public async Task<ActionResult> AllBanks(int page = 1)
        {
            IEnumerable<Bank> banks = (await _databaseManager.GetBanks()).Skip((page - 1) * 10).Take(10);
            IEnumerable<BankModel> banksModel = BankModel.AllBanks(banks);

            ViewBag.PageCount = (banks.Count() / 10) + 1;

            ViewBag.Message = TempData["deleted"];

            return View(banksModel);
        }

        [HttpGet]
        public async Task<ActionResult> EditBank(int id)
        {
            Bank bank = await _databaseManager.GetBank(id);
            BankModel bankModel = BankModel.GetBankModel(bank);
            return View(bankModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditBank(BankModel bankModel, HttpPostedFileBase Ico)
        {
            if (ModelState.IsValid)
            {
                IFileManager fileManager = new FileManager();

                var bank = await _databaseManager.GetBank(bankModel.Id);

                if (bank.Name != bankModel.Name)
                {
                    fileManager.RenameFile(bank.Name, bankModel.Name, Server.MapPath($"/Content/Images/Banks_Icons/{bank.Name.Trim(' ')}"));
                    bank.Name = bankModel.Name;
                }

                if (bank.Description != bankModel.Description)
                {
                    bank.Description = bankModel.Description;
                }


                string iconExtension = string.Empty;
                if (Ico != null)
                {
                    iconExtension = fileManager.SaveFile(Ico, Server.MapPath($"/Content/Images/Banks_Icons/{bank.Name.Trim(' ')}"));
                    bank.Ico = $"/Content/Images/Banks_Icons/{bank.Name}{iconExtension}";
                }

                await _databaseManager.ChangeBank(bank);

                TempData["givAMessage"] = true;
                return RedirectToAction("BankPage", new { id = bank.Id });
            }

            return View(bankModel);
        }

        [HttpGet]
        public async Task<ActionResult> BankPage(int id)
        {
            bool sentAMessage = (bool?)TempData["givAMessage"] ?? false;
            if (sentAMessage)
            {
                ViewBag.Message = Messages.BankSuccessfullyEdited;
            }

            Bank bank = await _databaseManager.GetBank(id);
            BankModel bankModel = BankModel.GetBankModel(bank);
            return View(bankModel);
        }

        [HttpGet]
        public async Task<ActionResult> DeleteBank(int id)
        {

            List<Deposit> deposits = await _databaseManager.GetDeposits();

            var dep = deposits.Where(d => d.BankId == id);

            if (dep.Count() > 0)
            {
                IEnumerable<Client> clients = await _databaseManager.GetClients();
                IEnumerable<Bank> banks = (await _databaseManager.GetBanks());
                TempData["BankId"] = id;
                var depositViewModel = DepositModel.GetAllDeposits(deposits.Where(d => d.BankId == id), clients, banks);

                return View("DeleteBank", depositViewModel.ToList());
            }

            await _databaseManager.DeleteBank(id);
            TempData["deleted"] = Messages.BankDeleted;
            return RedirectToAction("AllBanks");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteBank(List<DepositModel> depositModels)
        {
            var deposits = await _databaseManager.GetDeposits();
            var banks = await _databaseManager.GetBanks();
            int oldBankId = (int)TempData["BankId"];
            using (var dbTransaction = new VoloTaskDatabaseEntities().Database.BeginTransaction())
            {
                foreach (var item in depositModels)
                {
                    try
                    {
                        int newBankId = banks.Where(b => b.Name == item.Bank.Name).Last().Id;
                        var oldDeposit = deposits.Where(d => d.BankId == oldBankId && d.ClientId == item.Client.Id).Last();

                        oldDeposit.Status = DepositStatus.BankRetired.ToString();


                        Deposit newDeposit = new Deposit
                        {
                            BankId = newBankId,
                            ClientId = item.Client.Id,
                            DepositId = oldDeposit.DepositId,
                            Profit = oldDeposit.Profit,
                            Deadline = oldDeposit.Deadline,
                            StartDate = oldDeposit.StartDate,
                            Amount = oldDeposit.Amount,
                            Status = DepositStatus.Shifted.ToString(),
                        };

                        await _databaseManager.AddDeposit(newDeposit);
                        await _databaseManager.DeleteDeposit(oldDeposit.Id);
                    }
                    catch
                    {
                        dbTransaction.Rollback();
                        throw;
                    }
                }
                try
                {
                    await _databaseManager.DeleteBank(oldBankId);
                    dbTransaction.Commit();
                }
                catch
                {
                    dbTransaction.Rollback();
                    throw;
                }

                TempData["deleted"] = Messages.BankDeleted;
            }
            return RedirectToAction("AllBanks");
        }
    }
}