﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Volo.Task.Database.Entities;
using Volo.Task.Database.Services;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel
{
    public class BankModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Insert Bank's Name")]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Insert Bank's Description")]
        public string Description { get; set; }
        [ScaffoldColumn(false)]
        public string Ico { get; set; }


        /// <summary>
        /// The bank models transforms to BankViewModels
        /// </summary>
        /// <param name="banks"></param>
        /// <returns>BankViewModels</returns>
        public static IEnumerable<BankModel> AllBanks(IEnumerable<Bank> banks)
        {
            foreach (var item in banks)
            {
                BankModel bank = new BankModel();
                bank.Id = item.Id;
                bank.Name = item.Name;
                bank.Description = item.Description;
                bank.Ico = item.Ico;
                yield return bank;
            }
        }

        /// <summary>
        /// The Bank model transforms to BankViewModel
        /// </summary>
        /// <param name="bank"></param>
        /// <returns>BankViewModel</returns>
        public static BankModel GetBankModel(Bank bank)
        {
            return new BankModel
            {
                Id = bank.Id,
                Name = bank.Name,
                Description = bank.Description,
                Ico = bank.Ico
            };
        }

    }
}