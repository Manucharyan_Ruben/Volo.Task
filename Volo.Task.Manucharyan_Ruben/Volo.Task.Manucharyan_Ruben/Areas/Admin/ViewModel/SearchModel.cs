﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Volo.Task.Database.DepositTypes;
using Volo.Task.Database.Entities;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel
{
    public class SearchModel
    {
       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BankName { get; set; }
        public string DepositType { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
        public DateTime? MinStartDate { get; set; }
        public DateTime? MaxStartDate { get; set; }

        private List<string> depositTypes { get;  set; }

        public  List<string> DepositTypes
        {
            get
            {
                depositTypes = new List<string>() { "All" };
                foreach (var item in AllDeposits.GetAll)
                {
                    depositTypes.Add(item.Name);
                }

                return depositTypes;
            }
        }

        public List<SearchResult> SearchResult
        {
            get
            {
                return searchResult;
            }
        }

        private List<SearchResult> searchResult;


        public SearchModel(dynamic result)
        {
            searchResult = new List<SearchResult>();
            SearchResult searchModel;
            foreach (var item in result)
            {
                searchModel = new SearchResult(item.Bank, item.Client, item.Deposit);
                searchResult.Add(searchModel);
            }
        }

        public SearchModel()
        {

        }
    }

    public class SearchResult
    {
        public Bank Bank { get; set; }
        public Client Client { get; set; }
        public Deposit Deposit { get; set; }

        public SearchResult(Bank bank, Client client, Deposit deposit)
        {
            Bank = bank;
            Client = client;
            Deposit = deposit;
        }
    }
}