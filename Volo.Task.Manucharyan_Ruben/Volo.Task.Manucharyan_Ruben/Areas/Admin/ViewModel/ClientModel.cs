﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Task.Database.Entities;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel
{
    public class ClientModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required(ErrorMessage ="Insert Client's First Name")]
        [StringLength(50, MinimumLength = 3)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Insert Client's First Name")]
        [StringLength(50, MinimumLength = 3)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Insert Client's Birthday")]
        public DateTime Birthday { get; set; }
        [Required(ErrorMessage = "Insert Client's Passport")]
        [StringLength(50, MinimumLength = 6)]
        public string Passport { get; set; }
        [Required(ErrorMessage = "Insert Client's Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Insert Client's Phone Number")]
        [RegularExpression(@"^[- .]?\d{2}[- .]?\d{2}[- .]?\d{2}[- .]?\d{2}$", ErrorMessage = "Not a valid Phone number")]
        public string Phone { get; set; }

        /// <summary>
        /// The Client models transforms to ClientViewModels
        /// </summary>
        /// <param name="clients"></param>
        /// <returns>BankViewModels</returns>
        public static IEnumerable<ClientModel> GetAllClients(IEnumerable<Client> clients)
        {
            foreach (var item in clients)
            {
                ClientModel client = new ClientModel()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Birthday = item.Birthday,
                    Email = item.Email,
                    Passport = item.Passport,
                    Phone = item.Phone
                };
                
                yield return client;
            }
        }

        /// <summary>
        /// The Client model transforms to ClientViewModel
        /// </summary>
        /// <param name="client"></param>
        /// <returns>ClientViewModel</returns>
        public static ClientModel GetClientModel(Client client)
        {
            return new ClientModel
            {
                Id = client.Id,
                FirstName = client.FirstName,
                LastName = client.LastName,
                Birthday = client.Birthday,
                Email = client.Email,
                Passport = client.Passport,
                Phone = client.Phone
            };
        }
    }
}