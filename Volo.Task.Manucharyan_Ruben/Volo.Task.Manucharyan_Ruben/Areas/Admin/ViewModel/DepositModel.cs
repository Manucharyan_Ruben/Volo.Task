﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Volo.Task.Database.DepositTypes;
using Volo.Task.Database.Entities;

namespace Volo.Task.Manucharyan_Ruben.Areas.Admin.ViewModel
{
    public class DepositModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        public int BankId { get; set; }
        public int ClientId { get; set; }
        public int DepositId { get; set; }
        [Required(ErrorMessage = "Insert Amount")]
        public decimal Amount { get; set; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Select Deadline")]
        public DateTime Deadline { get; set; }
        public DateTime StartDate { get; set; }


        public Client Client { get; set; }
        public Bank Bank { get; set; }
        public IEnumerable<Bank> Banks { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public List<string> DepositTypes { get; set; }
        public DepositType DepositType { get; set; }

        public decimal Profit { get; set; }
        public string Status { get; set; }

        public DepositModel()
        {
            DepositTypes = new List<string> { "Select deposit type" };
            foreach (var item in AllDeposits.GetAll)
            {
                DepositTypes.Add(item.Name);
            }
        }

        /// <summary>
        /// The Deposits model transforms to DepositViewModels
        /// </summary>
        /// <param name="deposit"></param>
        /// <param name="clients"></param>
        /// <param name="banks"></param>
        /// <returns>DepositViewModels</returns>
        public static IEnumerable<DepositModel> GetAllDeposits(IEnumerable<Deposit> deposit, IEnumerable<Client> clients
            , IEnumerable<Bank> banks)
        {

            foreach (var item in deposit)
            {
                DepositModel depositModel = new DepositModel();

                depositModel.Id = item.Id;
                depositModel.BankId = item.BankId;
                depositModel.ClientId = item.ClientId;
                depositModel.DepositId = item.DepositId;
                depositModel.Amount = item.Amount;
                depositModel.Deadline = item.Deadline;
                depositModel.StartDate = item.StartDate;
                depositModel.Client = clients.Where(c => c.Id == item.ClientId).Last();
                depositModel.Bank = banks.Where(b => b.Id == item.BankId).Last();
                depositModel.DepositType = AllDeposits.GetAll.Where(dt => dt.Id == item.DepositId).Last();
                depositModel.Banks = banks;
                depositModel.Profit = item.Profit;
                depositModel.Status = item.Status??"Active";
                yield return depositModel;
            }
        }
    }

}