﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Task.Database.DepositTypes
{
    public class DepositType 
    {
       public DepositType(int id , string name, double interestRate, int minTerm, int maxTerm, decimal minAmount, decimal maxAmount)
        {
            Id = id;
            Name = name;
            InterestRate = interestRate;
            MinTerm = minTerm;
            MaxTerm = maxTerm;
            MinAmount = minAmount;
            MaxAmount = maxAmount;
        }

        public DepositType()
        {
        }
        public int Id { get; set; }

        public double InterestRate { get; set; }

        public decimal MaxAmount{ get; set; }

        public decimal MinAmount { get; set; }

        public int MaxTerm {get;set;}

        public int MinTerm{get;set;}

        public string Name { get; set; }
    }
}