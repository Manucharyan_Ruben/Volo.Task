﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volo.Task.Database.DepositTypes
{
    public  enum DepositStatus
    {
        BankRetired,
        Shifted,
        Deleted
    }
}
