﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Volo.Task.Database.DepositTypes
{
    public static class AllDeposits
    {
        private static IEnumerable<DepositType> allDepositTypes;

        static AllDeposits()
        {
            allDepositTypes = new List<DepositType>
            {
                new DepositType(1,"Cumulative",12,90,720,100000,2000000),
                new DepositType(2,"Family",13,120,548,50000,1500000),
                new DepositType(3,"Savings",9,30,365,150000,2500000)
            };
        }
        public static IEnumerable<DepositType> GetAll
        {
            get
            {
                return allDepositTypes;
            }
        }
    }
}