﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Task.Database.Entities;

namespace Volo.Task.Database.Services
{
    public class ClientRepository : IVoloDatabaseRepository<Client>
    {
        private VoloTaskDatabaseEntities db;

        public ClientRepository()
        {
            db = new VoloTaskDatabaseEntities();
        }
        public async System.Threading.Tasks.Task Create(Client model)
        {
            db.Clients.Add(model);
            await db.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var client = await db.Clients.FindAsync(id);
            if (client != null)
            {
                client.DeleteDate = DateTime.Now;
                await Update(client);
            }
        }

        public async Task<Client> GetModel(int id)
        {
            return await db.Clients.FindAsync(id);
        }

        public async Task<List<Client>> GetModels()
        {
            return await db.Clients.Where(m => m.DeleteDate == null).ToListAsync();
        }



        public async System.Threading.Tasks.Task Update(Client model)
        {
            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }


        public async Task<List<Client>> GetDeletedModels()
        {
            return await db.Clients.Where(m => m.DeleteDate != null).ToListAsync();
        }

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public async System.Threading.Tasks.Task DeletePhysically(int id)
        {
            var client = await db.Clients.FindAsync(id);
            if (client != null)
            {
                db.Clients.Attach(client);
                db.Clients.Remove(client);
                await db.SaveChangesAsync();
            }
        }
    }
}
