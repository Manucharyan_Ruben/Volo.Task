﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Task.Database.Entities;

namespace Volo.Task.Database.Services
{
    public class BankRepository : IVoloDatabaseRepository<Bank>
    {
        private VoloTaskDatabaseEntities db;

        public BankRepository()
        {
            db = new VoloTaskDatabaseEntities();
        }
        public async System.Threading.Tasks.Task Create(Bank model)
        {
            db.Banks.Add(model);
            await db.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var bank = await db.Banks.FindAsync(id);
            if (bank != null)
            {
                bank.DeleteDate = DateTime.Now;
                await Update(bank);
            }
        }

        public async Task<Bank> GetModel(int id)
        {
            return await db.Banks.FindAsync(id);
        }

        public async Task<List<Bank>> GetModels()
        {
            return await db.Banks.Where(m => m.DeleteDate == null).ToListAsync();
        }


        public async System.Threading.Tasks.Task Update(Bank model)
        {
            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }


        public async Task<List<Bank>> GetDeletedModels()
        {
            return await db.Banks.Where(m => m.DeleteDate != null).ToListAsync();
        }

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public async System.Threading.Tasks.Task DeletePhysically(int id)
        {
            var bank = await db.Banks.FindAsync(id);
            if (bank != null)
            {
                db.Banks.Attach(bank);
                db.Banks.Remove(bank);
                await db.SaveChangesAsync();
            }
        }
    }
}
