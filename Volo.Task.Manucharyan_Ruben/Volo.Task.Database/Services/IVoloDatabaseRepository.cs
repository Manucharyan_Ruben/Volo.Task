﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volo.Task.Database.Services
{
    public interface IVoloDatabaseRepository<TModel> : IDisposable where TModel : class
    {
        Task<List<TModel>> GetModels();
        Task<List<TModel>> GetDeletedModels();
        Task<TModel> GetModel(int id);
        System.Threading.Tasks.Task Create(TModel model);
        System.Threading.Tasks.Task Update(TModel model);
        System.Threading.Tasks.Task Delete(int id);

        System.Threading.Tasks.Task DeletePhysically(int id);

    }
}
