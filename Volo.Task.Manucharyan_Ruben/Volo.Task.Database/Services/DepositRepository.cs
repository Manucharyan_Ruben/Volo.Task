﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Task.Database.Entities;

namespace Volo.Task.Database.Services
{
    public class DepositRepository : IVoloDatabaseRepository<Deposit>
    {
        private VoloTaskDatabaseEntities db;

        public DepositRepository()
        {
            db = new VoloTaskDatabaseEntities();
        }
        public async System.Threading.Tasks.Task Create(Deposit model)
        {
            db.Deposits.Add(model);
            await db.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var deposit = await db.Deposits.FindAsync(id);
            if (deposit != null)
            {
                deposit.DeleteDate = DateTime.Now;
                await Update(deposit);
            }
        }

        public async System.Threading.Tasks.Task<Deposit> GetModel(int id)
        {
            return await db.Deposits.FindAsync(id);
        }

        public async Task<List<Deposit>> GetModels()
        {
            return await db.Deposits.Where(m => m.DeleteDate == null).ToListAsync();
        }

        public async System.Threading.Tasks.Task Update(Deposit model)
        {
            db.Entry(model).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }


        public async Task<List<Deposit>> GetDeletedModels()
        {
            return await db.Deposits.Where(m => m.DeleteDate != null).ToListAsync();
        }


        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public async System.Threading.Tasks.Task DeletePhysically(int id)
        {
            var deposit = await db.Deposits.FindAsync(id);
            if (deposit != null)
            {
                db.Deposits.Attach(deposit);
                db.Deposits.Remove(deposit);
                await db.SaveChangesAsync();
            }
        }
    }
}
