﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Task.Database.Entities;

namespace Volo.Task.Database.DatabaseManager
{
    public interface IDatabaseManager
    {
        System.Threading.Tasks.Task AddClient(Client client);
        System.Threading.Tasks.Task AddBank(Bank bank);
        System.Threading.Tasks.Task AddDeposit(Deposit deposit);
        System.Threading.Tasks.Task DeleteClient(int id);
        System.Threading.Tasks.Task DeleteBank(int id);
        System.Threading.Tasks.Task DeleteDeposit(int id);
        System.Threading.Tasks.Task ChangeClient(Client client);
        System.Threading.Tasks.Task ChangeBank(Bank bank);

        Task<List<Bank>> GetBanks();
        Task<List<Client>> GetClients();
        Task<List<Deposit>> GetDeposits();
        Task<Bank> GetBank(int id);
        Task<Client> GetClient(int id);
        Task<Deposit> GetDeposit(int id);
    }
}
