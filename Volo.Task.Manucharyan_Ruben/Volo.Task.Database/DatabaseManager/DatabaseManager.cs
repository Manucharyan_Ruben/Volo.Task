﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Task.Database.Entities;
using Volo.Task.Database.Services;

namespace Volo.Task.Database.DatabaseManager
{
    public class DatabaseManager : IDatabaseManager
    {

        private BankRepository bankDb;
        private ClientRepository clientDb;
        private DepositRepository depositDb;

        public DatabaseManager()
        {
            bankDb = new BankRepository();
            clientDb = new ClientRepository();
            depositDb = new DepositRepository();
        }

        public async System.Threading.Tasks.Task AddBank(Bank bank)
        {
            await bankDb.Create(bank);
        }

        public async System.Threading.Tasks.Task AddClient(Client client)
        {
            await clientDb.Create(client);
        }

        public async System.Threading.Tasks.Task AddDeposit(Deposit deposit)
        {
            await depositDb.Create(deposit);
        }

        public async System.Threading.Tasks.Task ChangeBank(Bank bank)
        {
            await bankDb.Update(bank);
        }

        public async System.Threading.Tasks.Task ChangeClient(Client client)
        {
            await clientDb.Update(client);
        }

        public async System.Threading.Tasks.Task DeleteBank(int id)
        {
            await bankDb.Delete(id);
        }

        public async System.Threading.Tasks.Task DeleteClient(int id)
        {
            await clientDb.Delete(id);
        }

        public async System.Threading.Tasks.Task DeleteDeposit(int id)
        {
            await depositDb.Delete(id);
        }

        public async Task<Bank> GetBank(int id)
        {
            return await bankDb.GetModel(id);
        }

        public async Task<List<Bank>> GetBanks()
        {
            return await bankDb.GetModels();
        }

        public async Task<Client> GetClient(int id)
        {
            return await clientDb.GetModel(id);
        }

        public async Task<Deposit> GetDeposit(int id)
        {
            return await depositDb.GetModel(id);
        }

        public async Task<List<Deposit>> GetDeposits()
        {
            return await depositDb.GetModels();
        }

        public async Task<List<Client>> GetClients()
        {
            return await clientDb.GetModels();
        }
    }
}
